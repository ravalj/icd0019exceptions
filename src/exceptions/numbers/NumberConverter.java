package exceptions.numbers;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class NumberConverter {

    private final String language;
    private final Properties properties;

    public NumberConverter(String lang) {
        this.language = lang;

        String filePath = String.format(
                "src/exceptions/numbers/numbers_%s.properties", lang);

        Properties properties = new Properties();
        this.properties = properties;

        FileInputStream languageFile = null;

        try {
            languageFile = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader
                    (languageFile, Charset.forName("UTF-8"));

            properties.load(reader);
        } catch (IOException e) {
            System.out.println(language + ".properties file doesn't exist.");
            throw new MissingLanguageFileException();
        } catch (IllegalArgumentException e) {
            System.out.println(language + ".properties file has a broken part.");
            throw new BrokenLanguageFileException();
        } finally {
            try {
                if (languageFile != null) {
                languageFile.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String numberInWords(Integer number) {
        int hundreds = number / 100;
        int tens = (number - hundreds * 100) / 10;
        int ones = number - (hundreds * 100 + tens * 10);
        String numberStringFormat = "";


        if (ones >= 0 && tens == 0 || ones == 0 && tens == 1) {
            int smallNumber = ones + tens * 10;
            if (checkIfNumberInProperties(smallNumber)) {
                numberStringFormat = properties.getProperty(String.valueOf(smallNumber));
            }
        }

        if (tens > 1 && ones > 0) {
            numberStringFormat = properties.getProperty(String.valueOf(ones));
        }

        if (hundreds > 0 && tens == 0 && ones == 0) {
            numberStringFormat = "";
        }

        if (tens == 1 && ones != 0) {
            if (language.equals("et")) {
                if (checkIfSuffixExistsInProperties("teen")) {
                    numberStringFormat = properties.getProperty(String.valueOf(ones)) +
                            properties.getProperty(String.valueOf("teen"));
                }
            } else if (language.equals("en")) {
                int teenNumber = tens * 10 + ones;

                if ((teenNumber < 14 || teenNumber == 15 ||teenNumber == 18)
                        && checkIfNumberInProperties(teenNumber)) {
                    numberStringFormat = properties.getProperty(String.valueOf(teenNumber));
                } else {
                    if (checkIfSuffixExistsInProperties("teen")) {
                        numberStringFormat = properties.getProperty(String.valueOf(ones)) +
                                properties.getProperty(String.valueOf("teen"));
                    }
                }
            }
        }

        if (tens > 1) {
            String tensNumberWord = "";
            if (language.equals("et")) {
                if (checkIfSuffixExistsInProperties("tens-suffix") &&
                checkIfSuffixExistsInProperties("tens-after-delimiter")) {
                    tensNumberWord = properties.getProperty(String.valueOf(tens)) +
                            properties.getProperty(String.valueOf("tens-suffix"));
                }
            } else if (language.equals("en")) {
                int tensNumber = tens * 10;
                if (tens <= 5 || tens == 8) {
                    if (checkIfNumberInProperties(tensNumber)) {
                        tensNumberWord += properties.getProperty(String.valueOf(tensNumber));
                    }
                } else if (checkIfSuffixExistsInProperties("tens-suffix")) {
                    tensNumberWord = properties.getProperty(String.valueOf(tens)) +
                            properties.getProperty(String.valueOf("tens-suffix"));
                }
            }

            if (ones > 0) {
                numberStringFormat = tensNumberWord +
                        properties.getProperty(String.valueOf("tens-after-delimiter")) +
                        numberStringFormat;
            } else {
                numberStringFormat += tensNumberWord + numberStringFormat;
            }
        }

        if (hundreds > 0) {
            String hundredsAfterDelimiter = " ";
            if (checkIfSuffixExistsInProperties("hundred") &&
            checkIfSuffixExistsInProperties("hundreds-before-delimiter")) {
                if (numberStringFormat.equals("")) {
                    hundredsAfterDelimiter = "";
                }
                numberStringFormat = properties.getProperty(String.valueOf(hundreds)) +
                        properties.getProperty(String.valueOf("hundreds-before-delimiter")) +
                        properties.getProperty(String.valueOf("hundred")) + hundredsAfterDelimiter +
                        numberStringFormat;
            }
        }
        return numberStringFormat;
    }


    public boolean checkIfNumberInProperties(Integer number) {
        if (!properties.containsKey(String.valueOf(number))) {
            System.out.println("Number doesn't exist in the properties file.");
            throw new MissingTranslationException();
        }
        return true;
    }

    public boolean checkIfSuffixExistsInProperties(String suffix) {
        if (!properties.containsKey(suffix)) {
            System.out.println("File is missing suffix.");
            throw new MissingTranslationException();
        }
        return true;
    }
}
